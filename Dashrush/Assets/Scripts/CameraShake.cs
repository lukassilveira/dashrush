﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShake : MonoBehaviour
{
    [SerializeField] Player player;

    [SerializeField] public float timer;
    [SerializeField] public float waitBeforeShake;
    [SerializeField] public float shakeTimeDash;


    [SerializeField] public float frequency;
    [SerializeField] public float amplitude;

    public CinemachineVirtualCamera virtualCamera;
    [SerializeField] public CinemachineBasicMultiChannelPerlin channelPerlin;

    void Start()
    {
        player = GetComponent<Player>();
        channelPerlin = virtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();    
    }

    public IEnumerator CameraShakingDash()
    {
        yield return new WaitForSeconds(player.firstWaitDash);
        channelPerlin.m_AmplitudeGain = amplitude;
        channelPerlin.m_FrequencyGain = frequency;
        yield return new WaitForSeconds(shakeTimeDash);
        channelPerlin.m_AmplitudeGain = 0;
        channelPerlin.m_FrequencyGain = 0;
    }
}
