﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallCollider : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
        var gm = FindObjectOfType<GameMaster>();
        gm.canScore = false;
        StartCoroutine(gm.GameOver());
    }
}
