﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBlock : MonoBehaviour
{
    [SerializeField] public GameMaster gameMaster;
    [SerializeField] public GameObject test;
    [SerializeField] Transform spawnPosition;
    [SerializeField] public int randomNumber;

    private void Start()
    {
        gameMaster = FindObjectOfType<GameMaster>();
        randomNumber = Random.Range(1, gameMaster.stageBlocks.Count);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "CoinCollector")
        {
            while (randomNumber == gameMaster.currentIndex)
            {
                randomNumber = Random.Range(1, gameMaster.stageBlocks.Count);
            }
            var newBlock = Instantiate(
                gameMaster.stageBlocks[randomNumber], 
                spawnPosition.position, 
                transform.rotation);
            newBlock.transform.position = new Vector3(newBlock.transform.position.x, 0, 0);
            gameMaster.currentIndex = randomNumber;
        }
    }
}
