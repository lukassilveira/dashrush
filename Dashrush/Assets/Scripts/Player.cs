﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ActionCode2D.Renderers;

public class Player : MonoBehaviour
{
    [Header("General")]
    [SerializeField] public float currentSpeed;
    [SerializeField] public float runningSpeed;
    [SerializeField] public AudioSource audio;
    [SerializeField] public AudioClip[] jumpClips;
    [SerializeField] public float jumpForce;
    [SerializeField] public float groundRadius;
    [SerializeField] public float distance;
    [SerializeField] public bool isInWall;
    [SerializeField] public bool canMove;
    [SerializeField] public bool isGrounded;
    [SerializeField] public Transform groundPosition;
    [SerializeField] public LayerMask ground;
    [SerializeField] public Vector3 wallOffset;
    [SerializeField] public Vector3 dashOffset;
    [SerializeField] public AudioClip[] coinClips;
    [SerializeField] BoxCollider2D col;
    [HideInInspector] Rigidbody2D rb;
    [HideInInspector] Animator anim;
    [SerializeField] SpriteRenderer sprite;
    [SerializeField] SpriteGhostTrailRenderer ghostTrail;

    [Header("Dash")]
    [SerializeField] public float dashSpeed;
    [SerializeField] public float firstWaitDash;
    [SerializeField] public float dashTime;
    [SerializeField] public bool canDash;
    [SerializeField] public bool isDashing;
    [SerializeField] public AudioClip dashClip;

    [Header("Death")]
    [SerializeField] public bool isAlive;
    [SerializeField] public float deathFlickerRate;
    [SerializeField] public int deathFlicks;
    [SerializeField] public AudioClip deathClip;

    [Header("References")]
    [SerializeField] public GameMaster gm;
    [SerializeField] public CameraShake cameraShake;
    [SerializeField] public Material normalMaterial;
    [SerializeField] public Material deathMaterial;
    [SerializeField] public ParticleSystem runningParticles;
    [SerializeField] public ParticleSystem dashingParticles;
    [SerializeField] public GameObject deathPrefab;
    [SerializeField] public Singleton singleton;
    [SerializeField] public AudioSource runningSource;
    [SerializeField] public Animator jumpButton;
    [SerializeField] public GameObject jumpTutorial;
    [SerializeField] public Animator dashButton;
    [SerializeField] public GameObject dashTutorial;
    [SerializeField] public GameObject tutorialHeader;

    private void Awake()
    {
        singleton = FindObjectOfType<Singleton>();
    }

    void Start()
    {
        currentSpeed = runningSpeed;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Move();
        DashTreatment();
        Ground();
        CheckForWallsAfterDash();
        ParticlesTreatment();
        AnimatorTreatment();

        if (Input.GetKeyDown(KeyCode.Z))
        {
            PlayerPrefs.SetInt("tutorial", 0);

        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            PlayerPrefs.SetInt("tutorial", 1);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            PlayerPrefs.SetInt("tutorial", 2);
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            Time.timeScale += .1f;
        }
    }

    void Move()
    {
        if (canMove) rb.velocity = new Vector2(currentSpeed, rb.velocity.y);
    }

    public void Jump()
    {
        if (isGrounded)
        {
            if (isAlive && PlayerPrefs.GetInt("tutorial") != 0)
            {
                jumpButton.SetTrigger("tutorialEnd");
                Destroy(jumpTutorial);

                if (PlayerPrefs.GetInt("tutorial") == 1) Time.timeScale = 1;
                var randomJumpSound = Random.Range(0, jumpClips.Length);
                if (singleton) if (singleton.isOn) audio.PlayOneShot(jumpClips[randomJumpSound], .4f);
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            }
        }        
    }

    public void Dash()
    {
        if (isAlive)
        {
            if (canDash && (PlayerPrefs.GetInt("tutorial") == 2 || PlayerPrefs.GetInt("tutorial") == 3))
            {
                dashButton.SetTrigger("tutorialEnd");
                Destroy(dashTutorial);
                Destroy(tutorialHeader);

                if (PlayerPrefs.GetInt("tutorial") == 2) Time.timeScale = 1;
                PlayerPrefs.SetInt("tutorial", 3);
                StartCoroutine(DashRoutine());
            }
        }
    }

    private void DashTreatment()
    {
        //col.enabled = !isDashing;

        Physics2D.IgnoreLayerCollision(8, 12, isDashing);
        Physics2D.IgnoreLayerCollision(13, 12, isDashing);


        if (!isDashing)
        {
            currentSpeed = Mathf.Lerp(rb.velocity.x, runningSpeed, 10f * Time.deltaTime);
        }

        if (isGrounded && !isDashing && isAlive) canDash = true;
    }

    void Ground()
    {
        isGrounded = Physics2D.OverlapCircle(groundPosition.position, groundRadius, ground);
    }

    void Ghost(bool value)
    {
        ghostTrail.enabled = value;
    }

    public void PlayCoinSound()
    {
        if (singleton) if (singleton.isOn)
        {
            var randomCoinSound = Random.Range(0, coinClips.Length);
            if (singleton.isOn) audio.PlayOneShot(coinClips[randomCoinSound], .5f);
        }
    }

    private void CheckForWallsAfterDash()
    {
        if (!isDashing) isInWall = Physics2D.BoxCast(transform.position + wallOffset, new Vector2(distance/3, distance), 90, Vector2.zero, distance/2, ground);
        if (isInWall && isAlive)
        {
            StartCoroutine(Death());
        }
    }

    private void ParticlesTreatment()
    {
        if (runningParticles)
        {
            ParticleSystem.EmissionModule runningEmission = runningParticles.emission;
            runningEmission.enabled = (isGrounded && !isDashing);
        }
        if (dashingParticles)
        {
            ParticleSystem.EmissionModule dashingEmission = dashingParticles.emission;
            dashingEmission.enabled = true;
        }
    }

    private void AnimatorTreatment()
    {
        anim.SetFloat("verticalSpeed", rb.velocity.y);
        anim.SetFloat("horizontalSpeed", rb.velocity.x);
        anim.SetBool("isDashing", isDashing);
        anim.SetBool("isGrounded", isGrounded);
    }

    IEnumerator DashRoutine()
    {
        canDash = false;
        isDashing = true;
        rb.constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;
        StartCoroutine(cameraShake.CameraShakingDash());
        yield return new WaitForSeconds(firstWaitDash);
        rb.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        if (singleton) if (singleton.isOn) audio.PlayOneShot(dashClip, .4f);
        rb.gravityScale = 0;
        transform.position += dashOffset;
        currentSpeed = dashSpeed;
        Ghost(true);
        yield return new WaitForSeconds(dashTime);
        isDashing = false;
        Ghost(false);
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        rb.gravityScale = 4;
        //currentSpeed = runningSpeed;
    }

    public IEnumerator Death()
    {
        Time.timeScale = 1;
        isAlive = false;
        rb.velocity = Vector2.zero;
        FindObjectOfType<BackgroundFollow>().GetComponent<BackgroundFollow>().enabled = false;
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
        Destroy(runningParticles);
        Destroy(dashingParticles);
        anim.SetTrigger("dying");
        for (int i = 0; i < deathFlicks; i++)
        {
            sprite.material = deathMaterial;
            yield return new WaitForSeconds(deathFlickerRate);
            sprite.material = normalMaterial;
            yield return new WaitForSeconds(deathFlickerRate);
        }
        StartCoroutine(gm.GameOver());
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Spike")
        {
            StartCoroutine(Death());
            //Destroy(gameObject, 2f);
        }
    }

    private void OnDestroy()
    {
        if (singleton) if (singleton.isOn)
        {
            if (deathClip) AudioSource.PlayClipAtPoint(deathClip, Camera.main.transform.position, .5f);
        }
        Instantiate(deathPrefab, transform.position, transform.rotation);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(groundPosition.position, groundRadius);
        Gizmos.DrawWireCube(transform.position + wallOffset, new Vector2(distance/3, distance));
    }
}
