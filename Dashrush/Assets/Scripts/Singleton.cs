﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour
{
    [SerializeField] public bool isOn;

    void Awake()
    {
        if (PlayerPrefs.GetInt("audio") == 1)
        {
            isOn = true;
        }
        else
        {
            isOn = false;
        }

        int count = FindObjectsOfType<Singleton>().Length;

        if (count > 1)
        {
            Destroy(gameObject);
        }

        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Update()
    {
        if (isOn)
        {
            GetComponent<AudioSource>().volume = 1;
            PlayerPrefs.SetInt("audio", 1);
        }
        else
        {
            GetComponent<AudioSource>().volume = 0;
            PlayerPrefs.SetInt("audio", 0);
        }
    }
}
