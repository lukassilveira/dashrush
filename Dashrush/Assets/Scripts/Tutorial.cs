﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public float slowTime;
    public float slowValue;

    public Animator jumpText;
    public Animator dashText;

    public Animator jumpButton;
    public Animator dashButton;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(PlayerPrefs.GetInt("tutorial") == 0);
        if (PlayerPrefs.GetInt("tutorial") == 0) Debug.Log("asdasd");
    }

    private void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (PlayerPrefs.GetInt("tutorial") == 0) StartCoroutine(SlowMotionJumpTutorial());
        if (PlayerPrefs.GetInt("tutorial") == 1) StartCoroutine(SlowMotionDashTutorial());
    }

    private IEnumerator SlowMotionJumpTutorial()
    {
        Time.timeScale = slowValue;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        yield return new WaitForSecondsRealtime(slowTime);
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(1);
        jumpButton.SetTrigger("tutorial");
        jumpText.SetTrigger("tutorial");
        yield return new WaitForSecondsRealtime(1);
        PlayerPrefs.SetInt("tutorial", 1);
    }

    private IEnumerator SlowMotionDashTutorial()
    {
        Time.timeScale = slowValue;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        yield return new WaitForSecondsRealtime(slowTime);
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(1);
        dashButton.SetTrigger("tutorial");
        dashText.SetTrigger("tutorial");
        yield return new WaitForSecondsRealtime(1);
        PlayerPrefs.SetInt("tutorial", 2);
    }
}
