﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Record : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(PlayerPrefs.GetFloat("savedScore") > 0);
        GetComponent<Text>().text = "Record: " + PlayerPrefs.GetFloat("savedScore").ToString("0");
    }
}
