﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    Material material;
    [SerializeField] private float speed;
    [SerializeField] private Animator fadeOut;
    [SerializeField] private Image audioButton;
    [SerializeField] private Singleton singleton;

    void Start()
    {
        if (PlayerPrefs.GetInt("tutorial") != 3) PlayerPrefs.SetInt("tutorial", 0);
        singleton = FindObjectOfType<Singleton>();
        material = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        material.mainTextureOffset += new Vector2(speed * Time.deltaTime, 0);
        UpdateAudioButton();
    }

    private void UpdateAudioButton()
    {
        if (PlayerPrefs.GetInt("audio") == 1)
        {
            audioButton.color = Color.white;
        }
        else
        {
            audioButton.color = new Color(1, 1, 1, .3f);
        }

    }

    public void ClickedPlay()
    {
        StartCoroutine(ClickedPlayRoutine());
    }

    public void ClickedAudioButton()
    {
        singleton = FindObjectOfType<Singleton>();
        singleton.isOn = !singleton.isOn;
    }

    public IEnumerator ClickedPlayRoutine()
    {
        fadeOut.SetTrigger("fadeOut");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(1);
    }
}
