﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundFollow : MonoBehaviour
{
    Player player;
    Material material;

    void Start()
    {
        player = FindObjectOfType<Player>();
        material = GetComponent<Renderer>().material;    
    }

    void Update()
    {
        if (player)
        {
            transform.position = new Vector3(player.transform.position.x + 18, transform.position.y, 10);
            material.mainTextureOffset += new Vector2(player.GetComponent<Rigidbody2D>().velocity.x / 50 * Time.deltaTime, 0);
        }
    }
}
