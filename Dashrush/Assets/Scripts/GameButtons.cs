﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameButtons : MonoBehaviour, IPointerDownHandler
{
    public enum button {DASH, JUMP};
    public button buttonEnum;
    public Player player;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (buttonEnum == button.DASH)
        {
            player.Dash();
            Debug.Log("Clicked dash!");
        }
        else
        {
            player.Jump();
            Debug.Log("Clicked jump!");
        }
    }
}
