﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour
{
    [SerializeField] public Player player;
    [Space]
    [SerializeField] public List<GameObject> stageBlocks;
    [SerializeField] public int currentIndex;
    [Space]
    [SerializeField] public float score;
    [SerializeField] public int scoreRate;
    [SerializeField] public TMP_Text scoreText;
    [Space]
    [SerializeField] public List<Animator> gameOverAnimators;
    [SerializeField] public Animator retryImage;
    [SerializeField] public Animator fadeInImage;
    [SerializeField] public Animator newRecord;
    [SerializeField] public bool canScore;
    [Space]
    [SerializeField] public int nextTargetScore;
    [SerializeField] public int amoutOfScoreToTarget;
    [SerializeField] public float timeIncrease;
    [Range(1.5f, 2)][SerializeField] public float maxTimeIncrease;
    [SerializeField] public Animator info;

    private void Start()
    {
        player = FindObjectOfType<Player>();
        fadeInImage.SetTrigger("fadeIn");
    }

    public void Update()
    {
        Score();
        CheckForPlayer();
        TimeIncreaseByScore();
    }

    public void CheckForPlayer()
    {
        if (!player)
        {
            StartCoroutine(GameOver());
        }
    }

    public void Score()
    {
        if (player.isAlive && canScore) score += scoreRate * Time.deltaTime;
        scoreText.text = "SCORE: " + score.ToString("0");
        //if (!player) scoreText.GetComponent<Animator>().SetTrigger("gameOver");
    }

    public void CoinCollected(int amount)
    {
        score += amount;
    }

    public void TimeIncreaseByScore()
    {
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0, maxTimeIncrease);
        if (score >= nextTargetScore && Time.timeScale < maxTimeIncrease && player.isAlive)
        {
            Debug.Log("Time increase");
            info.SetTrigger("speed");
            //scoreRate++;
            Time.timeScale += timeIncrease;
            nextTargetScore += amoutOfScoreToTarget;
        }
    }

    public void ClickedRetry()
    {
        StartCoroutine(ClickedRetryRoutine());
    }

    public void ClickedHome()
    {
        StartCoroutine(ClickedHomeRoutine());
    }

    public IEnumerator ClickedRetryRoutine()
    {
        retryImage.SetTrigger("retry");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public IEnumerator ClickedHomeRoutine()
    {
        retryImage.SetTrigger("retry");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(0);
    }

    public IEnumerator GameOver()
    {
        yield return new WaitForSeconds(.5f);
        if (score > PlayerPrefs.GetFloat("savedScore"))
        {
            PlayerPrefs.SetFloat("savedScore", score);
            newRecord.SetTrigger("newRecord");
        }
        foreach (Animator anim in gameOverAnimators)
        {
            anim.SetTrigger("gameOver");
        }
    }
}
