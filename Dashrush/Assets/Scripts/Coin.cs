﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] int amount;
    [SerializeField] private GameObject collectFX;
    GameMaster gameMaster;

    void Start()
    {
        gameMaster = FindObjectOfType<GameMaster>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "CoinCollector")
        {
            gameMaster.CoinCollected(amount);
            Instantiate(collectFX, transform.position, transform.rotation);
            collision.transform.parent.GetComponent<Player>().PlayCoinSound();
            Destroy(gameObject);
            //Debug.Log("Coin collected");
        }
    }
}
